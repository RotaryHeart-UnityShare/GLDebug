# UnityGLDebug

Independent class that can be used to draw lines using GL. Its used the same way that any Debug.DrawLine works. Start drawing lines that can be viewed on game (even built game) with just calling GLDebug.DrawLine or GLDebug.DrawRay.

The script will be attached to the MainCamera automatically since it needs to be on a camera to be able to draw the lines.

# Wiki

A detailed wiki can be found at the <a href="https://www.rotaryheart.com/Wiki/UnityGLDebug.html">Rotary Heart Wiki</a> section.

# Importing
Copy all the files to your project Asset folder or use Unity Package Manager git setup to import it. Add the following to your project packages.json `"rotaryheart.lib.gldebug": "https://gitlab.com/RotaryHeart-UnityShare/GLDebug.git"`

# IMPORTANT NOTE:
Don't forget to get the shaders from the code section!
