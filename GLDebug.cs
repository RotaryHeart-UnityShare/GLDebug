﻿//Based of the code provided here
//https://forum.unity.com/threads/drawline-with-gl.235793/#post-1566858

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

namespace RotaryHeart.Lib.UnityGLDebug
{
    [RequireComponent(typeof(Camera))]
    public class GLDebug : MonoBehaviour
    {
        /// <summary>
        /// Main class used to save line information
        /// </summary>
        private class LineDrawingData
        {
            /// <summary>
            /// The initial array capacity to use
            /// </summary>
            private const int Capacity = 50;

            /// <summary>
            /// Array of all the start positions for the lines
            /// </summary>
            public readonly List<Vector3> startPos = new List<Vector3>(Capacity);
            /// <summary>
            /// Array of all the end positions for the lines
            /// </summary>
            public readonly List<Vector3> endPos = new List<Vector3>(Capacity);
            /// <summary>
            /// Array of all the colors
            /// </summary>
            public readonly List<Color> colors = new List<Color>(Capacity);
            /// <summary>
            /// Array of all the start time
            /// </summary>
            public readonly List<float> startTime = new List<float>(Capacity);
            /// <summary>
            /// Array of all the duration
            /// </summary>
            public readonly List<float> durations = new List<float>(Capacity);

            /// <summary>
            /// Removes all the elements at the specified index
            /// </summary>
            /// <param name="i">The index to remove items from</param>
            public void RemoveAt(int i)
            {
                startPos.RemoveAt(i);
                endPos.RemoveAt(i);
                colors.RemoveAt(i);
                startTime.RemoveAt(i);
                durations.RemoveAt(i);
            }

            /// <summary>
            /// Adds new data to the arrays
            /// </summary>
            /// <param name="start">The world position where the line will start from</param>
            /// <param name="end">The world position where the line will end from</param>
            /// <param name="color">The color to draw the line</param>
            /// <param name="time">The time when the line started drawing</param>
            /// <param name="duration">How long should the line last</param>
            public void Add(Vector3 start, Vector3 end, Color color, float time, float duration)
            {
                startPos.Add(start);
                endPos.Add(end);
                colors.Add(color);
                startTime.Add(time);
                durations.Add(duration);
            }
        }

        /// <summary>
        /// Indicates if the system has been initialized
        /// </summary>
        private static bool M_initialized = false;
        /// <summary>
        /// The instance reference for the system
        /// </summary>
        private static GLDebug M_instance = null;

        /// <summary>
        /// Indicates if the lines should be drawn
        /// </summary>
        public bool displayLines = true;
        /// <summary>
        /// The shader to use for the depth test drawing
        /// </summary>
        public Shader zOnShader;
        /// <summary>
        /// The shader to use for the non depth test drawing
        /// </summary>
        public Shader zOffShader;

        /// <summary>
        /// Holds the information for the lines that are using depth test drawing
        /// </summary>
        private readonly LineDrawingData m_linesZOn = new LineDrawingData();
        /// <summary>
        /// Holds the information for the lines that are not using depth test drawing
        /// </summary>
        private readonly LineDrawingData m_linesZOff = new LineDrawingData();

        /// <summary>
        /// The depth test drawing material reference
        /// </summary>
        private Material m_matZOn = null;
        /// <summary>
        /// The non depth test drawing material reference
        /// </summary>
        private Material m_matZOff = null;

        /// <summary>
        /// Holds the instance of the GLDebug. If no instance is found adds one to the main camera
        /// </summary>
        public static GLDebug Instance
        {
            get
            {
                if (!M_initialized)
                {
                    Camera cam = Camera.main;

                    if (cam == null)
                    {
                        throw new System.Exception("Couldn't find any main camera to attach the GLDebug script. System will not work");
                    }

                    M_instance = cam.gameObject.AddComponent<GLDebug>();
                    M_initialized = true;
                }

                return M_instance;
            }
        }

        private void Awake()
        {
            //This is to allows the system to be attached to any camera
            if (M_instance == null)
            {
                M_instance = this;
                M_initialized = true;
            }
            //Special check to remove any duplicated component, only 1 of this component is required
            else if (M_instance != this)
            {
                Destroy(this);

                return;
            }

            MaterialSetup();
        }

        private void OnEnable()
        {
            RenderPipelineManager.endCameraRendering += OnCameraRender;
        }

        private void OnDisable()
        {
            RenderPipelineManager.endCameraRendering -= OnCameraRender;
        }

        /// <summary>
        /// Handles the materials setup
        /// </summary>
        private void MaterialSetup()
        {
            //Z on material
            m_matZOn = zOnShader == null ? new Material(Shader.Find("Debug/GLlineZOn")) : new Material(zOnShader);

            //Z off material
            m_matZOff = zOffShader == null ? new Material(Shader.Find("Debug/GLlineZOff")) : new Material(zOffShader);

            m_matZOn.hideFlags = HideFlags.HideAndDontSave;
            m_matZOn.shader.hideFlags = HideFlags.HideAndDontSave;
            m_matZOff.hideFlags = HideFlags.HideAndDontSave;
            m_matZOff.shader.hideFlags = HideFlags.HideAndDontSave;
        }

        /// <summary>
        /// BRP function that allows the system to work
        /// </summary>
        private void OnPostRender()
        {
            DrawLines();
        }

        /// <summary>
        /// URP function that allows the system to work
        /// </summary>
        private void OnRenderObject()
        {
            DrawLines();
        }

        /// <summary>
        /// URP function that allows the system to work
        /// </summary>
        private void OnCameraRender(ScriptableRenderContext context, Camera cam)
        {
            DrawLines();
        }

        /// <summary>
        /// Executes the actual line drawing
        /// </summary>
        private void DrawLines()
        {
            if (!displayLines)
            {
                return;
            }

            float time = Time.time;

            //Depth test lines
            int count = m_linesZOn.colors.Count;

            if (count > 0)
            {
                //Lines without depth test
                m_matZOn.SetPass(0);

                GL.Begin(GL.LINES);
                for (int i = count - 1; i >= 0; i -= 2)
                {
                    GL.Color(m_linesZOn.colors[i]);
                    GL.Vertex(m_linesZOn.startPos[i]);
                    GL.Vertex(m_linesZOn.endPos[i]);

                    if (time - m_linesZOn.startTime[i] >= m_linesZOn.durations[i])
                    {
                        m_linesZOn.RemoveAt(i);
                    }

                    int nextIndex = i - 1;
                    if (nextIndex >= 0)
                    {
                        GL.Color(m_linesZOn.colors[nextIndex]);
                        GL.Vertex(m_linesZOn.startPos[nextIndex]);
                        GL.Vertex(m_linesZOn.endPos[nextIndex]);

                        if (time - m_linesZOn.startTime[nextIndex] >= m_linesZOn.durations[nextIndex])
                        {
                            m_linesZOn.RemoveAt(nextIndex);
                        }
                    }
                }

                GL.End();
            }

            //Lines without depth test
            count = m_linesZOff.colors.Count;

            if (count > 0)
            {
                //Lines without depth test
                m_matZOff.SetPass(0);

                GL.Begin(GL.LINES);
                for (int i = count - 1; i >= 0; i -= 2)
                {
                    GL.Color(m_linesZOff.colors[i]);
                    GL.Vertex(m_linesZOff.startPos[i]);
                    GL.Vertex(m_linesZOff.endPos[i]);

                    if (time - m_linesZOff.startTime[i] >= m_linesZOff.durations[i])
                    {
                        m_linesZOff.RemoveAt(i);
                    }

                    int nextIndex = i - 1;
                    if (nextIndex >= 0)
                    {
                        GL.Color(m_linesZOff.colors[nextIndex]);
                        GL.Vertex(m_linesZOff.startPos[nextIndex]);
                        GL.Vertex(m_linesZOff.endPos[nextIndex]);

                        if (time - m_linesZOff.startTime[nextIndex] >= m_linesZOff.durations[nextIndex])
                        {
                            m_linesZOff.RemoveAt(nextIndex);
                        }
                    }
                }

                GL.End();
            }
        }

        /// <summary>
        /// Draw a line from start to end with color for a duration of time and with or without depth testing. If duration is 0 then the line is rendered 1 frame.
        /// </summary>
        /// <param name="start">Point in world space where the line should start.</param>
        /// <param name="end">Point in world space where the line should end.</param>
        /// <param name="color">Color of the line.</param>
        /// <param name="duration">How long the line should be visible for.</param>
        /// <param name="depthTest">Should the line be obscured by objects closer to the camera ?</param>
        public static void DrawLine(Vector3 start, Vector3 end, Color? color = null, float duration = 0, bool depthTest = false)
        {
            if (!Instance.displayLines)
            {
                return;
            }

            LineDrawingData lines = depthTest ? M_instance.m_linesZOn : M_instance.m_linesZOff;
            lines.Add(start, end, color ?? Color.white, duration > 0 ? Time.time : 0, duration);
        }

        /// <summary>
        /// Draw a line from start to start + dir with color for a duration of time and with or without depth testing. If duration is 0 then the ray is rendered 1 frame.
        /// </summary>
        /// <param name="start">Point in world space where the ray should start.</param>
        /// <param name="dir">Direction and length of the ray.</param>
        /// <param name="color">Color of the ray.</param>
        /// <param name="duration">How long the ray should be visible for.</param>
        /// <param name="depthTest">Should the ray be obscured by objects closer to the camera ?</param>
        public static void DrawRay(Vector3 start, Vector3 dir, Color? color = null, float duration = 0, bool depthTest = false)
        {
            DrawLine(start, start + dir, color, duration, depthTest);
        }
    }
}